import { combineReducers, Reducer } from 'redux';


import propertyList from './routes/searchDemo/reducer';

import { handleActions } from 'redux-actions';
import * as Actions from './constants/actions';
import * as _ from 'lodash'


const initialLocationOptionsState = {
    states: [],
    countries: [],
    timeZones: []
}

const locationOptions = handleActions<any>({
    [Actions.GET_LOCATION_OPTIONS_API_SUCCESS]: (state, action) => {
        const newState = _.cloneDeep(state);
       
        newState.timeZones = action.payload.timeZones.map((timezone: any) => {
            return {id: timezone.id, name: timezone.displayName}
        })

        newState.states = action.payload.states.map((state: any) => {
            return {id: state.id, name: state.name, nameShort: state.nameShort}
        });
        
        newState.countries = action.payload.countries.map((country: any) => {
            return {id: country.id, name: country.name}
        });

        return newState 
    },
}, initialLocationOptionsState);


const initialTags: any = []

const tags = handleActions<any>({
    [Actions.GET_TAGS_API_SUCCESS]: (state, action) => {
        const tags = action.payload.tags;
        return tags 
    },
}, initialTags);

const initialMatchTypes: any = [];

const matchTypes = handleActions<any>({
    [Actions.GET_MATCHTYPES_API_SUCCESS]: (state, action) => {
		const matchTypes = action.payload.matchTypes
        return matchTypes; 
    },
}, initialMatchTypes);

export interface RootState {
    locationOptions: LocationOptions;
	tags: Tag[];
	matchTypes: MatchType[]
	propertyList: PropertyList,
}

export interface Tag {
	name: string,
	isFilter: boolean,
	isDomain: boolean,
	isOptional: boolean,
  isTag: boolean,
  type?: string,
}

export interface MatchType {
	defaultMatchType: boolean,
	defaultNegativeMatchType: boolean,
	negativeMatchType?: boolean,
	displayName:string,
	id: string,
	matchType?: boolean,
	internalName: string,
	digitalMarketingName: string
}

export default combineReducers<RootState>({
	locationOptions,
	tags,
	matchTypes,
	propertyList,
});
