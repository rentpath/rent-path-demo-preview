import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import mainSaga from '../mainSagas';
import demoPreviewListSagas from '../routes/searchDemo/sagas';

const saga = createSagaMiddleware();

export {
	logger,
	saga,
	mainSaga,
	demoPreviewListSagas,
};
