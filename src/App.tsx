import React from 'react';
import logo from './logo.svg';
import './App.css';
import SearchDemoComponent from './routes/searchDemo';

function App() {
  return (
    <SearchDemoComponent />
  );
}

export default App;
