// Three main parts to sagas file that make lifecycle of sagas to function together
// 1. our worker saga : calls the api and returns response 
// 2. our watcher saga : listening for actions to be dispatched. When it hears action getting called, it will call the worker to do its job
// 3. our root saga : take all listeners( watcher sagas ), and combine them into one root saga. When this runs, it will run all listeners at once. 

import { takeEvery } from 'redux-saga/effects';
import * as Actions from './constants/actions';
import { call, put } from 'redux-saga/effects';

import axios from 'axios';

export function* getProductsAsync(action:any):any {
    try {
        const response = yield call(axios.get, `${process.env.BASE_TEMPLATE_PATH}/templates/products`)

        yield put({
            type: 'PRODUCT_API_SUCCESS',
            payload: response.data
        })

    } catch (error) {
        console.log('GET PRODUCTS async ERROR', error)
    }
}

export function* getLocationOptionsAsync(action:any):any {
    try {  
        // countries hard coded for now
        const countriesResponse = yield { data: [{ id:  3322112234, name: "United States" }] };
        const countryId = countriesResponse.data[0].id;
        const timeZonesResponse = yield call(axios.get, process.env.BASE_PROPERTY_PATH+ `/properties/timezone`);
        const statesResponse = yield call(axios.get, process.env.BASE_PROPERTY_PATH+ `/properties/country/${countryId}/state`);
        

        yield put({
            type: Actions.GET_LOCATION_OPTIONS_API_SUCCESS,
            payload: {
                states: statesResponse.data,
                countries: countriesResponse.data,
                timeZones: timeZonesResponse.data
            }
        })

    } catch (error) {
        console.log('GET TIME ZONES async ERROR', error)
    }
}

export function* getTagsAsync(action:any):any {
    try {  
        
        const tagsResponse = yield call(axios.get, `${process.env.BASE_MANAGEMENT_PATH}/management/tags`);
        
        yield put({
            type: Actions.GET_TAGS_API_SUCCESS,
            payload: {
                tags: tagsResponse.data
            }
        })

    } catch (error) {
        console.log('GET TAGS async ERROR', error)
    }
}

export function* getMatchTypesAsync(action:any):any {
    try {  

        const matchTypesResponse = yield call(axios.get, process.env.BASE_BUILDOUT_PATH+ `/buildOut/matchTypes`);
        
        yield put({
            type: Actions.GET_MATCHTYPES_API_SUCCESS,
            payload: {
                matchTypes: matchTypesResponse.data
            }
        })

    } catch (error) {
        console.log('GET MATCH TYPES async ERROR', error)
    }
}

export function* getStructuredSnippetsHeaders(action:any):any {
    try {
        //TODO: The service doesn't use the param propertyId
        const response = yield call(axios.get, process.env.BASE_DIGITAL_MARKETING + `/digitalMarketing/property/NOT_DEFINED/snippet`)
        yield put({
            type: Actions.GET_STRUCUTRED_SNIPPETS_HEADERS_SUCCESS,
            payload: {
                data: response.data
            }
        })
    } catch (error) {
        console.log('STRUCTURED SNIPPET HEADER ERROR', error)
    }
}

export function* watchGetProducts() {
    yield takeEvery('GET_PRODUCTS_FROM_API', getProductsAsync)
}
export function* watchGetStructuredSnippetsHeaders() {
    yield takeEvery(Actions.GET_STRUCUTRED_SNIPPETS_HEADERS, getStructuredSnippetsHeaders)    
}
export function* watchGetLocationOptions() {
    yield takeEvery(Actions.GET_LOCATION_OPTIONS_API, getLocationOptionsAsync)    
}

export function* watchGetMatchTypes() {
    yield takeEvery(Actions.GET_MATCHTYPES_API, getMatchTypesAsync)    
}

export function* watchGetTags() {
    yield takeEvery(Actions.GET_TAGS_API, getTagsAsync)    
}

export default function* propertyInfoSaga() {
    yield [
        watchGetProducts(),
        watchGetLocationOptions(),
        watchGetMatchTypes(),
        watchGetTags(),
        watchGetStructuredSnippetsHeaders()
    ]
}