import { createStore, applyMiddleware, Store } from 'redux';

import { 
	logger, 
	saga,
	mainSaga,
	demoPreviewListSagas,
} from '../middleware/index';

import rootReducer, { RootState } from '../mainReducer';

export function configureStore(initialState?: RootState): Store<RootState> {
	const create = createStore;

	const createStoreWithMiddleware = applyMiddleware(logger, saga)(create);

	const store = createStoreWithMiddleware(rootReducer, initialState) as Store<RootState>;

	if (module.hot) {
		module.hot.accept('../mainReducer', () => {
			const nextReducer = require('../mainReducer');
			store.replaceReducer(nextReducer);
    	});
	}

	saga.run(mainSaga);
	saga.run(demoPreviewListSagas);
  return store;
}
