import styled from 'styled-components'

export const Container = styled.div`
    min-height: 200px;
    margin-top: 44px;
    display:flex;
    justify-content: center;
    align-items: center;
    padding: 40px;
`
export const EmptyStateContainer = styled.div`
    font-family: Roboto;
    display: flex;
    justify-content: center;
    padding: 10px;
    border: 1px solid #DDD;
    border-radius: 4px;
    background-color: #FFF;
    box-shadow: 0px 0px 17px 0px rgba(0,0,0,0.26);
    flex-direction: column;
    padding-bottom: 20px;
    
`
export const EmptyStateTitle = styled.h3`
    color: #999;
    font-family: Roboto;
    padding: 20px;
    font-weight: 600;
	font-size: 28px;
	letter-spacing: 1.5px;
    text-align: center;
`

export const EmptyStateDescription = styled.p`
    color: #999;
    font-family: Roboto;
    padding: 10px;
    font-size: 18px;
    letter-spacing: 1px;
    text-align: center;

`

export const EmptyStateHighlight = styled.span`
    color: tomato;
    font-size: 16px;
    display: inline-block;
    text-align: center;
    letter-spacing: 1px;
`

export const LoaderContainer = styled.div`
    width: 100%;
    height: 1000px;
    background: #000000;
    display: flex;
    align-items: center;
    justify-content: space-evenly;
`
