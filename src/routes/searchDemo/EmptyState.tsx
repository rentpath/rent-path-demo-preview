import * as React from 'react';
import {EmptyStateContainer, EmptyStateDescription, EmptyStateTitle, Container, EmptyStateHighlight} from './style'

interface EmptyStateProps {
    title: string
    message: string
    queryString: string;
}

export const EmptyState = (props: EmptyStateProps) => {
    return(
        <Container>
            <EmptyStateContainer>
                <EmptyStateTitle>
                    {
                        props.title
                    }
                </EmptyStateTitle>    
                <EmptyStateDescription>
                    {
                        props.message
                    }
                </EmptyStateDescription>   
                <EmptyStateHighlight>
                    {
                        props.queryString
                    }
                </EmptyStateHighlight>            
            </EmptyStateContainer>   
        </Container>
    );
}