import { createAction } from '@reduxjs/toolkit';
import * as Actions from '../../constants/actions';

export const getProperties = createAction<DemoPreviewListItem[]>(Actions.GET_PROPERTY_LIST_API);

