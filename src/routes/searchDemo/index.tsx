import * as React from 'react';
import * as _ from 'lodash';

import * as PropertyListActions from './actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { RootState } from '../../mainReducer';
import Table  from './Table'
import { PageMain } from './Page';

import { SOURCES } from '../../constants/sources';
import { STATUS, STATUS_MAPPINGS } from '../../constants/status';

import { Redirect } from 'react-router-dom'
  
import { EmptyState } from './EmptyState'
import { LoaderContainer } from './style';
import { ScreenLoaderComponent } from '../../components/screenLoader/index';

export const headerTitles = {
    DEMO_PREVIEW: { name: window.location.href.toString().toLowerCase().includes("rent_target")? "Target" : ( window.location.href.toString().toLowerCase().includes("rent_social") ? "Social" : "Search"), widthPercentageDelegation: 12 },
    LISTING_ID: { name: "Listing Id", widthPercentageDelegation: 12 },
    CLIENT_NAME: { name: "Client Name", widthPercentageDelegation: 20 },
    PMC: { name: "PMC", widthPercentageDelegation: 20 },
    CITY: { name: "City", widthPercentageDelegation: 12 },
    STATE: { name: "State", widthPercentageDelegation: 8 },
    ADDRESS: { name: "Address", widthPercentageDelegation: 20 }
}

export namespace PropertyList {
    export interface Props {
        totalProperties?: number,
        currentPage?: number,
        currentOffset?: number,
        properties?: DemoPreviewListItem[]
        getDemoPreviewList?: any,
        propertyListLoaded?: boolean,
        pageUrl: string,
    }

    export interface State { 
        offset: number
        limit: number
        searchQuery: string
        clickOnNewProperty: boolean
        clickOnRow: boolean
        itemClicked: DemoPreviewListItem
        onStatusClicked?: boolean,
        searchWords: string[];
    }
}

const mapStateToProps = (state: RootState) => {
    const properties = state.propertyList.properties;
    const totalProperties = state.propertyList.totalNumber;
    const propertyListLoaded = state.propertyList.propertyListLoaded;
    const pageUrl = state.propertyList.pageUrl
    return { 
        properties,
        totalProperties,
        propertyListLoaded,
        pageUrl
    };
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        getDemoPreviewList: bindActionCreators(PropertyListActions.getProperties as any, dispatch),
    };
}

class SearchDemoComponent extends React.Component<PropertyList.Props, PropertyList.State> {

    constructor(props: PropertyList.Props) {
        super(props);
        this.state = {
            itemClicked: {
                id: 0,
                listingId: 0,
                clientName: 'HI',
                address: "HI",
                state: "HI",
                pmc: "hi",
                city: "hi",
                previewUrl: "",
                leadmailUrl: ""
            },
            offset: 0,
            limit: 20,
            searchQuery: "",
            clickOnNewProperty: false,
            clickOnRow: false,
            searchWords: []
        }
        this.onReloadList = this.onReloadList.bind(this);
        this.onSearchQueryChange = this.onSearchQueryChange.bind(this);
        this.onNewPropertyClick = this.onNewPropertyClick.bind(this);
        this.onItemClicked = this.onItemClicked.bind(this);
        this.onItemStatusClicked = this.onItemStatusClicked.bind(this);
        this.setSearchWords = this.setSearchWords.bind(this);
    }

    componentDidMount() {
        this.props.getDemoPreviewList({offset: this.state.offset, limit: this.state.limit, pageUrl: this.props.pageUrl});
    }

    setSearchWords(searchWords: string[]) {
        this.setState({ searchWords });
    }

    onNewPropertyClick() { this.setState({clickOnNewProperty: true}) }

    onReloadList(offset: number, limit: number): void {
        if (this.state.searchWords.length > 0 ) {
            this.props.getDemoPreviewList({offset, limit, query: this.state.searchWords, pageUrl: this.props.pageUrl});
        } else {
            this.props.getDemoPreviewList({offset, limit, pageUrl: this.props.pageUrl});
        }
    }

    onSearchQueryChange(e: any): void {
        this.props.getDemoPreviewList({offset:0, limit:20,query: e.target.value, pageUrl: this.props.pageUrl});
        this.setState({ searchQuery: e.target.value});
    }

    onItemClicked(item: DemoPreviewListItem): void {
        this.setState({
            clickOnRow: true,
            onStatusClicked: false,
            itemClicked: item
        })
    }

    onItemStatusClicked(item: DemoPreviewListItem): void {
        this.setState({
            clickOnRow: true,
            onStatusClicked: true,
            itemClicked: item
        })
    }

    render(): JSX.Element {

        if (this.state.clickOnRow) {
            let path = `property/${this.state.itemClicked.id}`;        
        }

        const loadingState = <LoaderContainer> <ScreenLoaderComponent /> </LoaderContainer>;
        const emptyState = <EmptyState title={`Oops! 💁`} message={`No results found for this search:`} queryString={this.state.searchQuery}/>;

        const emptyListItem = this.props.propertyListLoaded ? emptyState : loadingState;

        return (
            <div>
                <Table 
                    searchFilters={this.state.searchWords}
                    setSearchWords={this.setSearchWords}
                    reloadList={this.onReloadList} 
                    totalItems={this.props.totalProperties ? this.props.totalProperties : 0} 
                    properties={this.props.properties ? this.props.properties : []}
                    onItemSelected={this.onItemClicked}
                    onStatusSelected={this.onItemStatusClicked}
                    headerTitles={headerTitles}
                    itemsPerPage={20}
                    pagesToShow={6}
                    emptyItem={emptyListItem}
                />
            </div>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SearchDemoComponent)
