// Three main parts to sagas file that make lifecycle of sagas to function together
// 1. our worker saga : calls the api and returns response 
// 2. our watcher saga : listening for actions to be dispatched. When it hears action getting called, it will call the worker to do its job
// 3. our root saga : take all listeners( watcher sagas ), and combine them into one root saga. When this runs, it will run all listeners at once. 

import * as Actions from '../../constants/actions';
import { takeEvery, call, put, all, fork } from 'redux-saga/effects';
import { PREVIEWDATA_PARAMS, LEADMAIL_PARAMS } from '../../constants/sql'
import axios from 'axios';

export function* getDemoPreviewList(action: any): any {
    try {
        const limit = action.payload.limit;// 20;
        const offset = action.payload.offset;//0;
        let query = "";
        let cols = "&cols=";
        if(action.payload.query !== undefined){
            query+="&query=";
            for (let index = 0; index < action.payload.query.length; index++) {
                const element = action.payload.query[index];
                query+= element;
                if(index+1 < action.payload.query.length)
                query+="**";
            }
            query=query.replace(/ /g,"__");
        }
        if(window.location.href.toString().toLowerCase().includes("rent_target")){
            cols += LEADMAIL_PARAMS.join("**");
        }else{
            cols += PREVIEWDATA_PARAMS.join("**");
        }
        const response: any =  yield call(axios.get, `${process.env.REACT_APP_BASE_INTEGRATION_PATH}/rentPathIntegration/previewData/list?limit=${limit}&offset=${offset}${query}${cols}`);
        yield put({
            type: Actions.GET_PROPERTY_LIST_API_SUCCESS,
            payload: {
                response: response.data
            }
        })
    } catch (error) {
        console.log('GET DEMO PREVIEW ATTRIBUTES async ERROR', error)
    }
}

export function* watchGetDemoPreviewList() {
    yield takeEvery(Actions.GET_PROPERTY_LIST_API, getDemoPreviewList)    
}

export default function* propertyInfoSaga() {
    yield all([ 
        watchGetDemoPreviewList(),
    ])
}