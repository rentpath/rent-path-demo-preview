
import styled from 'styled-components';
import * as ICONS from '../../../constants/iconCodes';

export const Page = styled.div`
    min-height: 900px;
    margin-bottom: 200px;
    width: 100%;
    float: left;
    background: #000000;
    border-radius: 4px;
    -webkit-box-shadow: 10px 10px 49px 7px rgba(191, 191, 191, 0.75);
    -moz-box-shadow: 10px 10px 49px 7px rgba(191, 191, 191, 0.75);
    box-shadow: 10px 10px 49px 7px rgba(191, 191, 191, 0.75);
`;

interface HeaderProps {
    mainBorderRadiusPixel:number
}

export const Header = styled.div`
    display:flex;
    height:96px;
    flex-direction: row;
    width: 100%;
    justify-content: space-between;
    align-items: center;
    background-image: linear-gradient(to bottom, #319ef0, #2289d7);
`;

export const Body = styled.div`
    width: 100%;
`;

export const HeaderTitle = styled.div`
    color: white;
    font-size: 24px;
    font-family: RobotoMedium;
    margin-left: 20px;
    letter-spacing: 1.5px;
`;
export const SearchInput = styled.input`
    font-family: Roboto;
    font-size: 21px;
    letter-spacing: 1px;
    text-align: left;
    color: #ffffff;
    background: none;
    outline: none;
    border: 1px solid transparent;
    border-bottom: 1px solid rgba(255,255,255, 0.5);
    margin-left: 10px;
    &::placeholder {
        opacity: 0.7;
        font-family: Roboto;
        font-size: 21px;
        letter-spacing: 1px;
        text-align: left;
        color: #ffffff;
    };
`
export const ActionGroup = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    padding: 20px 60px;
    margin: 10px 10px;
`
export const IconSearchGroup = styled.div`
    margin-left: 20px;
`

export const IconSearch = styled.span`
  &:before{
      /* use !important to prevent issues with browser extensions that change fonts */
      font-family: 'icomoon' !important;
      speak: none;
      font-style: normal;
      font-weight: normal;
      font-variant: normal;
      text-transform: none;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
      content: "\\${ICONS.LOOKING_GLASS}";
      font-size: 20px;
      color: #FFF;
      line-height: 20px;
  }
`;

export const NewButton = styled.div`
    border-radius: 17px;
    background-color: #ffffff;
    line-height: 34px;
    height:34px;
    color: #6697e6;
    font-family: RobotoMedium;
    font-size: 14px;
    padding: 2px 5px;
    width:116px;
    letter-spacing: 1px;
    text-align: center;
    margin-right:40px;
    cursor:pointer;
    transition: box-shadow 0.3s ease-in-out;
    &:before {
        font-family: 'icomoon' !important;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        content: "\\${ICONS.PLUS_ADD}";
        font-size: 14px;
        color: #6697e6;
        line-height: 20px;
        margin: 2px 8px 0 0;
    }
    :hover {
        background: #fff;
        box-shadow: 0 0 0 5px #1a70b3;
    };
`;

export const HeaderContainer = styled.div`
  display:flex;
  flex-direction: row;
  align-items: center;
  margin-right:20px;
`

export const SearchBox = styled.input`
    margin-left: 20px;
    transition: width 0.6s, border-radius 0.6s, background 0.6s, box-shadow 0.3s ease-in-out;
    width: 40px;
    height: 40px;
    border-radius: 20px;
    border: none;
    cursor: pointer;
    background: #fff;
    font-family: Roboto;
    font-size: 21px;
    letter-spacing: 1px;

    :hover {
        background: #fff;
        box-shadow: 0 0 0 5px #1a70b3;
    };
    :focus {
        transition: width 0.6s cubic-bezier(0, 1.22, 0.66, 1.39), border-radius 0.6s, background 0.6s;
        border: none;
        outline: none;
        box-shadow: none;
        padding-left: 15px;
        cursor: text;
        width: 300px;
        border-radius: auto;
        background: #fff;
        font-family: Roboto;
        color: #0379d2;
    };  

    :not(:focus) {
        text-indent: -5000px;
    }  
`;

export const SearchBoxLabel = styled.span`
    color: #145f9a;
    cursor: pointer;
    font-size: 1.4em;
    &:before{
        position:relative;
        left: -33px;
        top: 2px;  
        pointer-events: none;
    };
`;