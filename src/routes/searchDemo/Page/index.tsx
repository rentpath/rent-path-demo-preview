import * as React from 'react';
import {
    Page,
    Header,
    HeaderTitle,
    ActionGroup,
    IconSearch,
    IconSearchGroup,
    SearchInput,
    NewButton,
    SearchBox,
    SearchBoxLabel,
    Body
} from './style'
import { ButtonMain }  from '../../../components/buttons/ButtonMain'


export class PageMain extends React.Component<{
  title: string;
  searchQuery: string;
  onSearchQueryChange: any;
  onNewButtonClick: any;
}, {}> {

  constructor(props?: {
    title: string;
    searchQuery: string;
    onSearchQueryChange: any;
    onNewButtonClick: any;
  }) {
    super(props? props : {
      title: "string",
      searchQuery: "string",
      onSearchQueryChange: {},
      onNewButtonClick: {},
    });
  }

  render(): JSX.Element {

    const headerHeightPixel = 200;
    const mainBorderRadiusPixel = 4;
    const productContainerHeight = 66;
    
    return (
        <Header>
          <HeaderTitle> {this.props.title} </HeaderTitle>
          <div>
            <SearchBox value={this.props.searchQuery} onChange={this.props.onSearchQueryChange}/>
            <SearchBoxLabel className="icon-looking-glass"/>
          </div>
            <NewButton onClick={this.props.onNewButtonClick}>
              PROPERTY
            </NewButton>
        </Header>
    );
  }
}