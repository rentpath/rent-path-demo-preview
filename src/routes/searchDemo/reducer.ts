import { handleActions } from 'redux-actions';
import * as Actions from '../../constants/actions';
import * as _ from 'lodash'
// import { addAnalyticsToSourceStatus } from '../Property/utils';

export const initialState: PropertyList = {
    properties: [],
    propertyListLoaded: false,
    totalNumber: 0,
    pageUrl: window.location.href.toString().toLowerCase()
};

export default handleActions<any>({
    [Actions.GET_PROPERTY_LIST_API_SUCCESS]: (state, action) => {
        const items = action.payload.response.items;
        const totalNumber = action.payload.response.total
        const propertyList = items.map((propAPI: any) => {

            const cityName: string = propAPI.cityName ? propAPI.cityName.trim() : "";
            const stateName: string = propAPI.stateName ? propAPI.stateName.trim() : "";
            const postalCode: string = propAPI.postalCode ? propAPI.postalCode : "";

            let location: string;
            if(!cityName && !stateName && !postalCode) { location = "(no entry)" }
            else {
                location = `${cityName}, ${stateName}, ${postalCode}`
                if(location.substring(0, 4) === ", , ") { location = location.substring(4) }                                
                if(location.substring(0, 2) === ", ") { location = location.substring(2) }
                if(location.substring(location.length - 2) === ", ") {location = location.substring(0,location.length - 2) }
                location = location.replace(", ,", ",");
            }
            
            return {
                id: propAPI.id,
                listingId: propAPI.listingId,
                clientName: propAPI.clientName,
                previewUrl: propAPI.previewUrl ? propAPI.previewUrl : "(not assigned)",
                leadmailUrl: propAPI.leadmailUrl ? propAPI.leadmailUrl  : "(not assigned)",
                pmc: propAPI.pmc,
                city: propAPI.city,
                state: propAPI.state,
                address: propAPI.address,
            }
        })
        return {
            ...state,
            properties: propertyList,
            totalNumber,
            propertyListLoaded: true
        }
    },
    [Actions.GET_PROPERTY_LIST_API]: (state, action) => {
        return {
            ...state,
        }
    }
}, initialState)