import styled from 'styled-components'

export const ListItem = styled.li`
    list-style: none;
    margin: 2px 5px;
    display:block;
`;

export const ListItemContent = styled.div`
    display:flex;
    padding-left: 2.5%;
    justify-content: space-evenly;
    min-height: 40px;
    background-color: #ffffff;
    box-shadow: 0 2px 4px 0 rgba(98, 98, 98, 0.15);
    border-radius: 2px;
    border: 1px solid #ecf0f5;
    align-items: center;
    transition: all 0.30s ease;
    &: hover {
        background-color: #f3f6f7;
        cursor: pointer;
    }
    flex-basis: 90%;
`;

interface ListItemCellProps {
    widthPercentage: number;
}

export const ListItemCell = styled.div`
    font-family: Roboto;
	font-size: 14px;
	letter-spacing: 1px;
	text-align: left;
    color: #000000;
    width: ${(props: ListItemCellProps) => props.widthPercentage - 5}%;
    margin-right: 5%;
    word-wrap: break-word;
`;

export const StatusContainer = styled.div`
    font-family: Roboto;
    font-size: 14px;
    letter-spacing: 1.4px;
    text-align: left;
    color: #7f80b8;
    flex-grow: 1;
    flex-basis: 10%;
    display:flex;
    flex-direction:row;
    align-items: center;
    justify-content: center;
    display:flex;
    padding-left: 10px;
    justify-content: space-evenly;
    min-height: 66px;
    background-color: #ffffff;
    box-shadow: 0 2px 4px 0 rgba(98, 98, 98, 0.15);
    border-radius: 2px;
    border: 1px solid #ecf0f5;
    align-items: center;
    transition: all 0.30s ease;
    margin-left:10px;
    &:hover {
        background-color: #f3f6f7;
        cursor: pointer;
    }
`
export const StatusSource = styled.div`
    display:flex;
    flex-direction:column;
    margin: 0px 10px;
    
`

export const SourceLogo = styled.span`
    display:block;
    text-align: center;
`

export const SourceStatusIcon = styled.span`
    display:block;
    text-align: center;
`
export const API_ERROR_ICON = styled.span`
    color: #F88574;
`
export const BUILD_ERROR_ICON = styled.span`
    color: #f88574;
`
export const SUCCESS_ICON = styled.span`
    color: #1bd1bc;
`

export const LoadingIndicatorWrapper  = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: space-around;
    align-items: center;
`;

export const UrlHref = styled.a`
    color: #4561EC;
    text-decoration:none
`;