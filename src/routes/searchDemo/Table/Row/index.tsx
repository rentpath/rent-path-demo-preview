import * as React from 'react';
import { connect } from 'react-redux';
import { RootState } from '../../../../mainReducer';

import { ListItem, ListItemContent, ListItemCell, UrlHref} from './style';
import {StatusCell} from './StatusCell';

import { headerTitles } from '../../index';

export namespace ListRow {
    export interface Props {
        item: any
        onListItemClicked: (item: any) => void
        onStatusClicked: (item: any) => void
        pageUrl?: string
    }

    export interface State {

    }
}

const mapStateToProps = (state: RootState) => {
    const pageUrl = state.propertyList.pageUrl;
    return { 
        pageUrl,
    };
}

class ListRowComponent extends React.Component<ListRow.Props, ListRow.State> {

    constructor(props: ListRow.Props) {
        super(props);
        this.onItemClick = this.onItemClick.bind(this);
        this.onStatusClicked = this.onStatusClicked.bind(this);
        this.parseUrl = this.parseUrl.bind(this);
    }

    onItemClick(e: any) {        
        this.props.onListItemClicked(this.props.item);
    }

    onStatusClicked(e: any) {        
        this.props.onStatusClicked(this.props.item);
    }


    parseUrl(previewUrl: string, leadmailUrl: string) {
        const url = previewUrl && previewUrl !== '(not assigned)' ?  previewUrl : leadmailUrl;

        if(url === "(not assigned)") return '#'

        const productMap = {
            "rent_social": "RentSocial",
            "social": "RentSocial",
            "rent_search": "RentSearch",
            "search": "RentSearch",
            "rent_target": "RentTarget"
        };

        const pageUrl = this.props.pageUrl;

        const productKey = Object.keys(productMap).find(key => {
            return pageUrl?.includes(key);
        });

        const product = productMap[productKey as keyof typeof productMap];

        return `${url}/${product || productMap.rent_search}`
    }

    
    render(): JSX.Element | null{
        return (
            <ListItem >
                <ListItemContent onClick={this.onItemClick}>
                    <ListItemCell widthPercentage={headerTitles.DEMO_PREVIEW.widthPercentageDelegation}>
                        <UrlHref href={this.parseUrl(this.props.item.previewUrl, this.props.item.leadmailUrl)} target="_blank">Demo</UrlHref>
                    </ListItemCell>
                    <ListItemCell widthPercentage={headerTitles.LISTING_ID.widthPercentageDelegation}>
                        { this.props.item.listingId }
                    </ListItemCell>
                    <ListItemCell widthPercentage={headerTitles.CLIENT_NAME.widthPercentageDelegation}>
                        { this.props.item.clientName }
                    </ListItemCell>
                    <ListItemCell widthPercentage={headerTitles.PMC.widthPercentageDelegation}>
                        { this.props.item.pmc }
                    </ListItemCell>
                    <ListItemCell widthPercentage={headerTitles.CITY.widthPercentageDelegation}>
                        { this.props.item.city }
                    </ListItemCell>
                    <ListItemCell widthPercentage={headerTitles.STATE.widthPercentageDelegation}>
                        { this.props.item.state }
                    </ListItemCell>  
                    <ListItemCell widthPercentage={headerTitles.ADDRESS.widthPercentageDelegation}>
                        { this.props.item.address }
                    </ListItemCell>
                </ListItemContent> 
            </ListItem>
        );
    }
}

export default connect(mapStateToProps, null)(ListRowComponent)
