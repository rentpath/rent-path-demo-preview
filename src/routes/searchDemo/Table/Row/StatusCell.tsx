import * as React from 'react';
import { StatusContainer, StatusSource, SourceLogo, SourceStatusIcon,API_ERROR_ICON, BUILD_ERROR_ICON,SUCCESS_ICON, LoadingIndicatorWrapper } from './style'
import { SOURCES } from '../../../../constants/sources';
import { STATUS, STATUS_MAPPINGS } from '../../../../constants/status';

import { LoadingIndicatorComponent } from '../../../../components/loadingIndicator/index';

export namespace ListRow {
    export interface Props {
        sourceStatus: SourceStatus[],
        onClick: (e: any) => void;
        status: string,
        uniqueId: number
    }

    export interface State {}
} 

export class StatusCell extends React.Component<ListRow.Props, ListRow.State> {

    constructor(props: ListRow.Props) {
        super(props);
        this.renderSourcesStatus = this.renderSourcesStatus.bind(this);
    }

    getImageForStatus(status: string): JSX.Element | null {
        if(STATUS_MAPPINGS[status] === STATUS.ERROR_API) { return <API_ERROR_ICON className={"icon-api-errors"} />; }
        else if (STATUS_MAPPINGS[status] === STATUS.ERROR_BUILD_OUT) { return <BUILD_ERROR_ICON className={"icon-failed-build"} />; }
        else if (STATUS_MAPPINGS[status] === STATUS.SUCCESS) { return <SUCCESS_ICON className={"icon-success-build"} />; }
        return null;
    }

    getImageForSource(source: string): string {
        const adwordsImage: string = require('./../../../../assets/img/aw_logo@2x.png');
        const analyticsImage: string = require('./../../../../assets/img/ga_logo@2x.png');
        const facebookImage: string = require('./../../../../assets/img/fb_logo@2x.png');

        if (source === SOURCES.ADWORDS) { return adwordsImage; } 
        else if (source === SOURCES.ANALYTICS) { return analyticsImage; } 
        else if (source === SOURCES.FACEBOOK) { return facebookImage }
        return '';
    }

    renderSourcesStatus(): JSX.Element[] {
        return this.props.sourceStatus.map(source => {

            const imageSrc: string = this.getImageForSource(source.sourceInternalName);
            const statusImg: JSX.Element | null = this.getImageForStatus(source.status);

            return(
                <StatusSource key={`status_source_propertyId_${this.props.uniqueId}_source_${source.sourceId}`}>
                    <SourceLogo>
                        { <img src={imageSrc} width={'23px'} height={'23px'} /> }
                    </SourceLogo>
                    { source.status === "IN_PROGRESS" ?
                        <LoadingIndicatorWrapper>
                            <LoadingIndicatorComponent color={"#6697E6"} />
                        </ LoadingIndicatorWrapper>
                        :
                        <SourceStatusIcon>
                            { statusImg }
                        </SourceStatusIcon>
                    }
                </StatusSource>
            )
        })
    }

    render(): JSX.Element {
        return (
            <StatusContainer onClick={this.props.onClick}>
                { this.renderSourcesStatus() }
            </StatusContainer>
        );
    }
}
