import * as React from 'react';

import { PagerComponent } from  '../../../components/Pager';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as PropertyListActions from '../actions';


import { ListHeaderComponent } from './Header';
import ListRowComponent from './Row';
import { ListContainer, Container, ScrollContainer, MainContainer} from './style';
import { InsideHeader, Title, SearchBox, Icon, SearchBoxIcon, Search, SearchContainer, PillsContainer, Pill, PillText, SelectedLabel, IconClose } from './SearchBar/style';

export namespace List {
    export interface Props {
        getDemoPreviewList?: any,
        properties: DemoPreviewListItem[]
        totalItems: number; 
        headerTitles: any;
        reloadList: (offset: number, limit: number) => void
        pagesToShow: number,
        itemsPerPage: number,
        emptyItem: JSX.Element,
        onItemSelected: (element: any) => void
        onStatusSelected: (element: any) => void,
        searchFilters: string[],
        setSearchWords: (value: string[]) => void;
    }

    export interface State {
        searchQuery: string;
    }
}

const mapStateToProps = (state: any) => {

    return { 
    };
}

const mapDispatchToProps = (dispatch: any) => {
    return {
        getDemoPreviewList: bindActionCreators(PropertyListActions.getProperties as any, dispatch),
    };
}

let resetPager = false;

export class Table extends React.Component<List.Props, List.State> {
    
    constructor(props: List.Props) {
        super(props);
        this.state = {
            searchQuery: "",
        }
        this.onPageChange = this.onPageChange.bind(this);
        this.onClickClose = this.onClickClose.bind(this);
        resetPager = false;
    }

    componentDidUpdate(){
        resetPager = false;
        console.log("RESETPAGER",resetPager)
    }

    onPageChange(pageNumber: number) {
        const offset = pageNumber * this.props.itemsPerPage;
        this.props.reloadList(offset, this.props.itemsPerPage)
    }

    onClickClose(value: string) {
        const newFilters: string[] = this.props.searchFilters.filter(filter => filter != value);
        this.props.setSearchWords(newFilters);
        this.props.getDemoPreviewList({offset: 0, limit: 20, query:newFilters});
        resetPager = true;
    }

    onEnterSearchBar(e: any){
        if(e.key === "Enter"){
            if(e.target.value !== ""){
                const newFilters: string[] = this.props.searchFilters;
                newFilters.push(e.target.value);
                this.props.setSearchWords(newFilters);
                this.props.getDemoPreviewList({offset: 0, limit: 20, query:newFilters});
                resetPager = true;
                this.setState({searchQuery:""})
            }
        }
    }

    render(): JSX.Element | null{        
        return (
            <Container>
                <ScrollContainer>                    
                    <MainContainer>
                        <SearchContainer>
                            <InsideHeader>
                                <Title> SELECT A CLIENT</Title>
                            </InsideHeader>
                            <SearchBox >
                                {
                                    <SearchBoxIcon>
                                        <Icon className="icon-looking-glass" />
                                    </SearchBoxIcon>
                                }
                                <Search 
                                    placeholder="Search by Listing ID, Client Name, PMC, City, State, Address" 
                                    value={this.state.searchQuery} 
                                    onChange={(e)=>this.setState({searchQuery:e.target.value})} 
                                    onKeyDown={(e)=>this.onEnterSearchBar(e)} />
                            </SearchBox>
                            {
                                this.props.searchFilters.length > 0 &&
                                <PillsContainer>
                                    <SelectedLabel>Filtered By</SelectedLabel>
                                    {
                                    this.props.searchFilters.map((filter, j) => (
                                        <Pill key={`PillSearchFilter_${j}`}>
                                        <PillText>{filter}</PillText>
                                        <IconClose className="icon-close" onClick={(event) => this.onClickClose(filter)} />
                                        </Pill>
                                    ))
                                    }
                                </PillsContainer>
                            }
                        </SearchContainer>
                        <ListHeaderComponent headerTitles={this.props.headerTitles}/>
                        <ListContainer>
                            {
                                this.props.properties.map((proprety) => {
                                    if(window.location.href.toString().toLowerCase().includes("rent_target") ){
                                        if(proprety.leadmailUrl !== "(not assigned)" && proprety.leadmailUrl !== ""){
                                            return (
                                                <ListRowComponent 
                                                    key={proprety.id}
                                                    item={proprety}
                                                    onListItemClicked={this.props.onItemSelected}
                                                    onStatusClicked={this.props.onStatusSelected}
                                                    
                                                    />
                                            );
                                        }
                                    }else{
                                        if(proprety.previewUrl !== "(not assigned)" && proprety.previewUrl !== ""){
                                            return (
                                                <ListRowComponent 
                                                    key={proprety.id}
                                                    item={proprety}
                                                    onListItemClicked={this.props.onItemSelected}
                                                    onStatusClicked={this.props.onStatusSelected}
                                                    
                                                    />
                                            );
                                        }
                                    }
                                    return null
                                })
                            }
                        </ListContainer>
                    </MainContainer>
                    <PagerComponent 
                        onPageChange={this.onPageChange} 
                        totalItems={this.props.totalItems} 
                        itemsPerPage={this.props.itemsPerPage} 
                        pagesToShow={this.props.pagesToShow} 
                        resetPager={resetPager}
                        />
                </ScrollContainer>
            </Container>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Table)
