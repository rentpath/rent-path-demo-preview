import styled from 'styled-components'

interface HeaderItemProps {
    widthPercentage: number;
}


export const HeaderItem = styled.li`
    list-style: none;
    font-weight: 500;
    width: ${(props: HeaderItemProps) => props.widthPercentage - 5}%;
    margin-right: 5%;
`
export const HeaderList = styled.ul`
    list-style: none;
    font-family: Roboto;
	font-size: 14px;
	letter-spacing: 1.1px;
	text-align: left;
    color: #ffffff;   
    height: 25px; 
    align-items: center;
    margin-left: 2.5%;
    padding-left: 5px;
    width: calc(100% - 20px);
    display: flex;
`;

export const HeaderStatus = styled.div`
    display:flex;
    margin:0;
    font-family: Roboto;
    font-size: 16px;
    letter-spacing: 1.3px;
    text-align: left;
    color: #ffffff;   
    height: 33px; 
    align-items: center;
    padding-left: 30px;
    padding-right: 30px;
    flex-basis: 10%;
`
export const HeaderItemContent = styled.div`
`
export const Container = styled.div`    
    padding: 2px 8px; 
    display: flex;
    background: #4561EC;
`