import * as React from 'react';
import * as _ from 'lodash';

import {
    HeaderItem, 
    HeaderItemContent, 
    Container, 
    HeaderList, 
    HeaderStatus
} from './style';

export namespace ListHeader {
    export interface Props {
        headerTitles: any
    }

    export interface State {}
}

export class ListHeaderComponent extends React.Component<ListHeader.Props, ListHeader.State> {

    constructor(props: ListHeader.Props) {
        super(props);
    }

    render(): JSX.Element | null{

        let headers: JSX.Element[] = [];

        _.forEach(this.props.headerTitles, (value, header) => {
            headers.push(
                <HeaderItem key={header} widthPercentage={value.widthPercentageDelegation}> 
                    { value.name }
                </HeaderItem>
            );
        })

        return (
            <Container>
                <HeaderList>
                    { headers }
                </HeaderList>
            </Container>
        );
    }
}
