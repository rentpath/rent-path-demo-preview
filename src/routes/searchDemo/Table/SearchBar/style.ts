import styled from 'styled-components';
import { COLOR_PALETTE } from '../../../../constants/colors';


export const SearchContainer = styled.div`
  background-color: #ffffff;
  padding: 0 0 15px 0;
`;

export const SearchBox = styled.div`
  width: 95%;
  height: 45px;
  border-radius: 2px;
  box-shadow: 0 2px 4px 0 rgba(98, 98, 98, 0.15);
  border: solid 0.5px #e2d4d4;
  background-color: #ffffff;
  margin: 5px auto auto auto;
  display: flex;
  align-items: center;
`;

export const Title = styled.div`
  display:flex;
  height: 100%;
  width: auto;
  align-items: center;
  font-family: Roboto;
  color: #cad3e0;
  font-size: 16px;
  font-weight: 600;
  letter-spacing: 4px;
`;

export const InsideHeader = styled.div`
  margin: auto;
  display:flex;
  height: 60px;
  width: 95%;
  justify-content: space-between;
`;

export const SearchBoxIcon = styled.div`
  -moz-transform: scale(-1, 1);
  -webkit-transform: scale(-1, 1);
  -o-transform: scale(-1, 1);
  -ms-transform: scale(-1, 1);
  transform: scale(-1, 1);
  width: 30px;
  height: 20px;
`;

export const Icon = styled.span`
  color: #868f9d;
  cursor: pointer;
  font-size: 18px;
  font-weight: bold;
  &:before{
    position:relative;
    left: 0px;
    top: 2px;  
    pointer-events: none;
  };
`;

export const Search = styled.input`
  margin-left: 20px;
  background: #868f9d;
  width: 500px;
  height: 40px;
  border-radius: 20px;
  border: none;
  cursor: pointer;
  background: #fff;
  font-family: Roboto;
  font-size: 14px;
  letter-spacing: 1px;
  :focus {
   outline: none;
  };  
  ::placeholder {
    color: #868f9d;
    opacity: 0.8;
  }
  :-ms-input-placeholder { 
    color: #868f9d;
  }
  ::-ms-input-placeholder { 
    color: #868f9d;
  }
`;

export const PillsContainer = styled.div`
  margin: 0 0 23px 26px;
`;

export const Pill = styled.div`
  display: inline-flex;
  width: 149px;
  height: 14px;
  line-height: 26px;
  border-radius: 3px;
  border: solid 2px #b7d2ff;
  background-color: ${COLOR_PALETTE.BLUE_LIGHT_8};
  font-family: Roboto;
  font-size: 14px;
  letter-spacing: 1px;
  padding: 5px 10px 6px 6px;
  align-items: center;
  justify-content: space-between;
  margin: 0 22px 5px 0;
`;

export const PillText = styled.span`
  width: 120px;
  color: ${COLOR_PALETTE.GREY_DARK_1};
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const IconClose = styled.span`
  color: ${COLOR_PALETTE.GREY_DARK_1};
  cursor: pointer;
  font-size: 10px;
  font-weight: normal;
  &:before{
    position:relative;
    left: 0px;
    top: 2px;  
    pointer-events: none;
  };
`;

export const SelectedLabel = styled.div`
  height: 14px;
  font-family: Roboto;
  font-size: 12px;
  letter-spacing: 0.75px;
  color: ${COLOR_PALETTE.GREY_LIGHT_5};
  margin: 17px 0 13px 0px;
`;