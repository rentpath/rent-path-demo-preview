import styled from 'styled-components'

export const ListContainer = styled.ul`
    margin: 0;
    list-style: none;
    padding: 4px 0px;
    background-color: #fff;
    padding-left: 10px;
    padding-right: 10px;
    // height:100vh;
    // overflow-y:scroll;
`
export const MainContainer = styled.div`
    margin: 25px 20px 0px 20px;
    -webkit-box-shadow: rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 2px 6px 2px;
    -moz-box-shadow: rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 2px 6px 2px;
    box-shadow: rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 2px 6px 2px;
`

export const Container = styled.div`    
    background-color: #000000;
    
`
export const ScrollContainer = styled.div`
    height: calc(100%);    
    overflow-y: scroll;
`