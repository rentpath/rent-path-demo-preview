import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route,  Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { configureStore } from './store';
import App from './App';
import './assets/icomoon/style.css';
import './assets/img/favicon.ico';
// import registerServiceWorker from './registerServiceWorker';
import 'react-dates/initialize';

const customHistory = createBrowserHistory();

export const store = configureStore();


ReactDOM.render(
	<Provider store={store}>
		<Router history={customHistory}>
			<Route path="/" component={App}/>
			
		</Router>
	</Provider>,
	document.getElementById('root')
);
