export const SOURCES = {
    ADWORDS: "ADWORDS",
    ANALYTICS: "ANALYTICS",
    FACEBOOK: "FACEBOOK"
};

// TODO: This is what API accepts. We are mixing up source and product.
// This needs to get fixed, FE, API, FL
export const API_FLATTENED_SOURCES = {
    ANALYTICS: 'ANALYTICS',
    ADWORDS: 'ADWORDS',
    FACEBOOK: 'FACEBOOK',
    SOCIAL_ADS_EXPRESS: 'SOCIAL_ADS_EXPRESS'
};