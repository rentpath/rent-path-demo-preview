export const PREVIEWDATA_PARAMS = [
    'listingId','clientName','pmc','city','state','address','previewUrl'
]

export const LEADMAIL_PARAMS = [
    'listingId','clientName','pmc','city','state','address','leadmailUrl'
]