import styled, { keyframes } from 'styled-components';

  /* Animations */
 const rotate = keyframes`
    from {
      -webkit-transform: rotate(0deg);
              transform: rotate(0deg);
    }
    to {
      -webkit-transform: rotate(360deg);
              transform: rotate(360deg);
    }
`;

const rotate_left = keyframes`
    from {
      -webkit-transform: rotate(16deg);
              transform: rotate(16deg);
    }
    to {
      -webkit-transform: rotate(376deg);
              transform: rotate(376deg);
    }
`;

const rotate_right = keyframes`
    from {
      -webkit-transform: rotate(4deg);
              transform: rotate(4deg);
    }
    to {
      -webkit-transform: rotate(364deg);
              transform: rotate(364deg);
    }
`;

  
export const Loader = styled.div`
    height: 400px;
    width: 400px;
    position: relative;
`;

export const LoaderCogs = styled.div`
    width: 100px;
    height: 100px;
    top: -120px !important;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    margin: auto;
`;

export const LoaderCogsTop = styled.div`
    position: relative;
    width: 100px;
    height: 100px;
    -webkit-transform-origin: 50px 50px;
            transform-origin: 50px 50px;
    -webkit-animation: ${rotate} 10s infinite linear;
            animation: ${rotate} 10s infinite linear;
    div:nth-of-type(1) {
        -webkit-transform: rotate(30deg);
                transform: rotate(30deg);    
    }
    div:nth-of-type(2) {
        -webkit-transform: rotate(60deg);
                transform: rotate(60deg);
    }
    div:nth-of-type(3) {
        -webkit-transform: rotate(90deg);
                transform: rotate(90deg);
    }
`;

export const TopPart = styled.div`
    width: 100px;
    border-radius: 6px;
    position: absolute;
    height: 100px;
    background: #26c3fe;
`;

export const TopHole = styled.div`
    width: 50px;
    height: 50px;
    border-radius: 100%;
    background: white;
    position: absolute;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    margin: auto;
`;

export const LoaderCogsLeft = styled.div`
    position: relative;
    width: 80px;
    -webkit-transform: rotate(16deg);
            transform: rotate(16deg);
    top: 28px;
    -webkit-transform-origin: 40px 40px;
            transform-origin: 40px 40px;
    animation: ${rotate_left} 10s .1s infinite reverse linear;
    left: -24px;
    height: 80px;
    div:nth-child(1) {
    -webkit-transform: rotate(30deg);
            transform: rotate(30deg);
    };
    div:nth-child(2) {
    -webkit-transform: rotate(60deg);
            transform: rotate(60deg);
    };
    div:nth-child(3) {
    -webkit-transform: rotate(90deg);
            transform: rotate(90deg);
    };
`;

export const LeftPart = styled.div`
    width: 80px;
    border-radius: 6px;
    position: absolute;
    height: 80px;
    background: #319EF0;
`;

export const LeftHole = styled.div`
    width: 40px;
    height: 40px;
    border-radius: 100%;
    background: white;
    position: absolute;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    margin: auto;
`;

export const LoaderCogsBottom = styled.div`
    position: relative;
    width: 60px;
    top: -65px;
    -webkit-transform-origin: 30px 30px;
            transform-origin: 30px 30px;
    -webkit-animation: ${rotate_left} 10s .4s infinite linear;
            animation: ${rotate_left} 10s .4s infinite linear;
    -webkit-transform: rotate(4deg);
            transform: rotate(4deg);
    left: 79px;
    height: 60px;
    div:nth-of-type(1) {
    -webkit-transform: rotate(30deg);
            transform: rotate(30deg);
    };
    div:nth-of-type(2) {
    -webkit-transform: rotate(60deg);
            transform: rotate(60deg);
    };
    div:nth-of-type(3) {
    -webkit-transform: rotate(90deg);
            transform: rotate(90deg);
    };
`;

export const BottomPart = styled.div`
    width: 60px;
    border-radius: 3px;
    position: absolute;
    height: 60px;
    background: #145F9A;
`;

export const BottomHole = styled.div`
    width: 30px;
    height: 30px;
    border-radius: 100%;
    background: white;
    position: absolute;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    margin: auto;
`;
