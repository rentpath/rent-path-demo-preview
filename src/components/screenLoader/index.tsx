import * as React from 'react';

import {
    Loader,
    LoaderCogs,
    LoaderCogsTop,
    LoaderCogsLeft,
    LoaderCogsBottom,
    TopPart,
    TopHole,
    LeftPart,
    LeftHole,
    BottomPart,
    BottomHole
} from './style';

export namespace ScreenLoader {
    export interface Props { }
    export interface State { }
}

export class ScreenLoaderComponent extends React.Component<ScreenLoader.Props> {
    
    constructor(props: ScreenLoader.Props) {
        super(props);
    }
    
    render() {
        return (
            <Loader>
                <LoaderCogs>
                    <LoaderCogsTop>
                        <TopPart />
                        <TopPart />
                        <TopPart />
                        <TopHole />
                    </LoaderCogsTop>
                    <LoaderCogsLeft>
                        <LeftPart />
                        <LeftPart />
                        <LeftPart />
                        <LeftHole />
                    </LoaderCogsLeft>
                    <LoaderCogsBottom>
                        <BottomPart />
                        <BottomPart />
                        <BottomPart />
                        <BottomHole />
                    </LoaderCogsBottom>
                </LoaderCogs>
            </Loader>
          
        );
    }
}