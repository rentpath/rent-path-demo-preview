import * as React from 'react';
import styled from 'styled-components';

export namespace ButtonMain {
    export interface Props {
        disabled: boolean;
        disabledBackgroundColor: string;
        disabledTextColor: string;
        hoverColor: string | null;
        hoverTextColor?: string | null;
        width: number;
        height: number;
        backgroundColor: string;
        textColor: string;
        iconText?: string | null;
        iconSize?: number;
        mainText: string;
        textSize: number;
        textFont: string;
        padding?: number;
        letterSpacing?: number;
        borderRadius?: number;
        onClickHandler: () => any;
        lineHeight?: number;
        animated?: boolean;
    }
}

export class ButtonMain extends React.Component<ButtonMain.Props> {

  constructor(props: ButtonMain.Props) {
    super(props);
    this.returnIconOption = this.returnIconOption.bind(this);
    this.onClickHandler = this.onClickHandler.bind(this);
  }

  returnIconOption(): JSX.Element | null{
    const Icon = styled.span`
        transition: all 0.25s;
        &:before{
            /* use !important to prevent issues with browser extensions that change fonts */
            font-family: 'icomoon' !important;
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            content: "\\${this.props.iconText}";
            font-size: ${this.props.iconSize}px;
            color: currentColor;
            line-height: ${this.props.lineHeight}px;
        }
    `;
    if(this.props.iconText){ return <Icon></Icon> }
    return null;
  }

    onClickHandler(e: any) {
        e.stopPropagation();
        this.props.onClickHandler();
  }
  render() {
    let hoverColor, hoverTextColor, borderRadius, letterSpacing;

    if (!this.props.hoverColor) {
        hoverColor = this.props.backgroundColor;
        hoverTextColor = this.props.textColor
    } else if (this.props.hoverTextColor) {
        hoverColor = this.props.hoverColor;
        hoverTextColor = this.props.hoverTextColor;  
    } else {
        hoverColor = this.props.hoverColor;
        hoverTextColor = this.props.textColor
    }

    if(this.props.borderRadius){
        borderRadius = this.props.borderRadius;
    } else {
        borderRadius = 2;
    }

    if(this.props.letterSpacing){
        letterSpacing = this.props.letterSpacing;
    } else {
        letterSpacing = 1;
    }

    const ButtonTextWrap = styled.span`
        margin-left: 4px;
    `;

    this.returnIconOption();
    const ButtonContainer = styled.button`    
        width: ${this.props.width}px;
        height: ${this.props.height}px;
        border-radius: ${borderRadius}px;
        border: none;
        background-color: ${this.props.backgroundColor};
        outline: none;
        color: ${this.props.textColor};
        letter-spacing: ${letterSpacing}px;
        font-family: ${this.props.textFont};
        font-size: ${this.props.textSize}px;
        display: flex;
        padding: ${this.props.padding}px;
        justify-content: space-around;
        align-items: center;
        line-height: ${this.props.lineHeight}px;
        transition: all 0.25s;
        margin-right: 19px;
        &:hover:enabled {
            background-color: ${hoverColor};
            color: ${hoverTextColor};
            cursor: pointer;
        };
        &:disabled {
            background: ${this.props.disabledBackgroundColor};
            color: ${this.props.disabledTextColor};
            user-select: none;
        };
    `;
    
    const ButtonContainerAnamated =  styled(ButtonContainer)`
        &:hover ${ButtonTextWrap} {
            transform: translateX(2px);
        }
       &:hover span:first-child {
            transform: scale(1.2);
        }
        &:disabled {
            background: ${this.props.disabledBackgroundColor};
            color: ${this.props.disabledTextColor};
            user-select: none;
        }
        &:hover:disabled span:first-child {
            transform:initial;
        }
        &:hover:disabled ${ButtonTextWrap} {
            transform: initial;
        }
    `; 

    if(!this.props.animated) {
        return (
            <ButtonContainer onClick={this.onClickHandler} disabled={this.props.disabled}>
                { this.returnIconOption() }
                <ButtonTextWrap> {this.props.mainText} </ButtonTextWrap>
            </ButtonContainer>
        );
    } else {
        return (
            <ButtonContainerAnamated onClick={this.onClickHandler} disabled={this.props.disabled}>
                { this.returnIconOption() }
                <ButtonTextWrap> {this.props.mainText} </ButtonTextWrap>
            </ButtonContainerAnamated>
        );
    }
  }
}