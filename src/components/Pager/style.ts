import styled from 'styled-components'

export const Container = styled.div`
    display:flex;
    align-items: center;
    justify-content: center;
    margin: 20px;
    
`
export const PagerContainer = styled.ul`
    list-style: none;
    margin-left: 8px;
    margin-right: 8px;
`

interface PagerItemProps {
    isSelected: boolean
}
export const PagerItem = styled.li`
    text-align: center; 
    display:inline-block;
    margin: 4px 4px; 
    width: 20px;
    padding: 5px 6px 4px;
	height: 20px;
	border-radius: 2px;
    font-family: Roboto;
	font-size: 14px;
    letter-spacing: 1.4px;
    font-weight: ${(props: PagerItemProps) => props.isSelected ? `bold` : `300`}; 
    color: ${(props: PagerItemProps) => props.isSelected ? `white` : `#6697e6`};
    background: ${(props: PagerItemProps) => props.isSelected ? `linear-gradient(to bottom,#4561EC,#4561EC)` : `#ffffff`};
	box-shadow: ${(props: PagerItemProps) => props.isSelected ? `1px 2px 4px 0 rgba(0, 0, 0, 0.10)` : `1px 2px 3px 0 rgba(0, 0, 0, 0.05)`};
`
export const PagerItemContent = styled.a`
    display: flex;
    width: 100%;
    padding: 0 1px; 
    justify-content: center;
    height: 100%;
    cursor: pointer;
`