export const PAGER_ELEMENT = {
    NEXT_SET: 'NEXT_SET',
    PREVIOUS_SET:'PREVIOUS_SET',
    NEXT_PAGE: 'NEXT_PAGE',
    PREVIOUS_PAGE: 'PREVIOUS_PAGE',
    FIRST_PAGE: 'FIRST_PAGE',
    LAST_PAGE: 'LAST_PAGE',
}

export interface Controls {
    action: string,
    display:string
}