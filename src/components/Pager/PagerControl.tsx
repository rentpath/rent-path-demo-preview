import * as React from 'react';
import { Controls } from './constants'
import { PagerItem, PagerItemContent } from './style'

interface PagerControlProps {
    control: Controls
    onPagerItemClicked: (action: string) => void;
    isSelected: boolean
}

export const PagerControl = (props: PagerControlProps) => {
    return(
        <PagerItem isSelected={props.isSelected} onClick={(e) => props.onPagerItemClicked(props.control.action)}>
            <PagerItemContent>
                {
                    props.control.display
                }
            </PagerItemContent>    
        </PagerItem>
    );
}