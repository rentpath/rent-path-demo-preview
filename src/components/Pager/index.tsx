import * as React from 'react';
import * as _ from 'lodash'
import { PagerItem, PagerContainer, Container, PagerItemContent} from './style'
import { PAGER_ELEMENT, Controls} from './constants'
import { PagerControl } from './PagerControl'

export namespace Pager {
    export interface Props {
        pagesToShow: number;
        totalItems: number;
        itemsPerPage: number
        resetPager?: boolean;
        onPageChange: (newPage: number) => void;
    }

    export interface State {
        currentPageNumber: number,
    }
}

export class PagerComponent extends React.Component<Pager.Props, Pager.State> {

    constructor(props: Pager.Props) {
        super(props);
        this.state = {
            currentPageNumber: 0,
        }
        this.onPagerItemClicked = this.onPagerItemClicked.bind(this);
        this.getCurrentPageSet = this.getCurrentPageSet.bind(this);
        this.getPagesInCurrentPageSet = this.getPagesInCurrentPageSet.bind(this);
        this.getTotalPageSet = this.getTotalPageSet.bind(this);
    }

    componentWillReceiveProps(props: Pager.Props) {
        if(props.resetPager) { this.setState({currentPageNumber: 0}) }
    }

    onPagerItemClicked(action: any) {
        const pagesToShow = this.props.pagesToShow;
        const totalItems = this.getTotalPages();
        let currentPageNumber = this.state.currentPageNumber;
        const currentPageSet = this.getCurrentPageSet(currentPageNumber);
        switch (action) {
            case PAGER_ELEMENT.NEXT_SET: {
                currentPageNumber = (currentPageSet + 1) * pagesToShow;
                break;
            }
            case PAGER_ELEMENT.PREVIOUS_SET: {
                currentPageNumber = (currentPageSet - 1) * pagesToShow;
                break;
            }
            case PAGER_ELEMENT.NEXT_PAGE: {
                currentPageNumber = Math.min(this.state.currentPageNumber+1, totalItems - 1)
                break;
            }
            case PAGER_ELEMENT.PREVIOUS_PAGE: {
                currentPageNumber = Math.max(this.state.currentPageNumber - 1,0)
                break;
            }
            case PAGER_ELEMENT.LAST_PAGE: {
                currentPageNumber = totalItems -1
                break;
            }
            case PAGER_ELEMENT.FIRST_PAGE: {
                currentPageNumber = 0
                break;
            }
            default: {
                const pageSelected = _.toNumber(action) - 1;
                currentPageNumber = pageSelected
                break;
            }
        }

        this.setState({ currentPageNumber })
        this.props.onPageChange(currentPageNumber);
    }

    getCurrentPageSet(currentPageNumber: any) {
        return (currentPageNumber < this.props.pagesToShow) ? 0 : Math.floor(currentPageNumber / this.props.pagesToShow);
    }

    getPagesInCurrentPageSet(currentPageNumber: any, currentPageSet: any, totalPageSet: any, totalItems: any) {
        const totalPages = this.getTotalPages()
        const pagesToShow = this.props.pagesToShow;

        if ((totalPages % (pagesToShow) !== 0) &&  (currentPageNumber >= ((totalPageSet - 1 ) * pagesToShow))) {
                const rest = totalPages - ((totalPageSet -1 ) * pagesToShow) 
                return _.times(rest, (i) => ((totalPageSet -1 ) * pagesToShow) + i + 1);   
        }
        
        return _.times(Math.min(totalPages,pagesToShow), (i) => (currentPageSet * pagesToShow) + i + 1);
    }

    getTotalPageSet() {
        const totalPages = this.getTotalPages()
        const pagesToShow = this.props.pagesToShow;
        return (totalPages < pagesToShow ? 1 : 0) + Math.floor(totalPages / pagesToShow) + (totalPages % pagesToShow !== 0 ? 1 : 0);
    }

    getTotalPages() {
        return Math.floor(this.props.totalItems / this.props.itemsPerPage) + (this.props.totalItems % this.props.itemsPerPage === 0 ? 0 : 1);
    }

    renderControls(controlList: Controls[]) : JSX.Element {
        return (
            <PagerContainer>
                    {
                        controlList.map((control) => {
                            return (
                                <PagerControl key={control.action} isSelected={true} onPagerItemClicked={this.onPagerItemClicked} control={control} />
                            )
                        })
                    }
            </PagerContainer>
        )
    }

    renderPageSetNav(action: Controls) {
        return (
            <PagerControl isSelected={false} onPagerItemClicked={this.onPagerItemClicked} control={action} />
        );
    }

    render(): JSX.Element | null {
        const totalPages = this.getTotalPages();
        const currentPageNumber = this.state.currentPageNumber;
        const totalPageSet = this.getTotalPageSet();
        const currentPageSet = this.getCurrentPageSet(currentPageNumber);
        const pagesInPageSet = this.getPagesInCurrentPageSet(currentPageNumber, currentPageSet, totalPageSet, this.props.totalItems);

        const prevControls = [
                // {action: PAGER_ELEMENT.FIRST_PAGE, display: "<<" },
                {action: PAGER_ELEMENT.PREVIOUS_PAGE, display: "<" }
        ]
        const nextControls = [
                {action: PAGER_ELEMENT.NEXT_PAGE, display: ">" },
                // {action: PAGER_ELEMENT.LAST_PAGE, display: ">>" }
        ]
        return (
            <Container>
                {
                    this.renderControls(prevControls)
                }
                <PagerContainer>
                    {
                        currentPageSet > 0 &&
                            this.renderPageSetNav({action: PAGER_ELEMENT.PREVIOUS_SET, display: "•••"})
                    }
                    {
                        pagesInPageSet.map((i) => {
                            const pageNumber = i
                            const label= ""+(pageNumber);
                            return (
                                <PagerItem key={i} isSelected={(pageNumber) === currentPageNumber + 1} onClick={(e) => this.onPagerItemClicked(label)}>
                                    <PagerItemContent>
                                    {
                                        label
                                    }
                                    </PagerItemContent>
                                </PagerItem>
                            )
                        })
                    }
                    {
                        this.props.pagesToShow < totalPages && 
                        currentPageSet * this.props.pagesToShow < totalPages - this.props.pagesToShow &&
                            this.renderPageSetNav({action: PAGER_ELEMENT.NEXT_SET, display: "•••"})
                    }
                </PagerContainer>
                {
                    this.renderControls(nextControls)
                }
            </Container>
        );
    }
}
