import styled from 'styled-components';

export const Container = styled.div`
	display:flex;
	box-shadow: 0 2px 7px 0 rgba(0, 0, 0, 0.04);
	justify-content: space-between;
`;

export const UserContainer = styled.div`
	text-align: right;
`;

export const UserText = styled.span`
	font-family: Roboto;
	font-size: 14px;
	font-weight: 500;
	font-style: normal;
	font-stretch: normal;
	line-height: normal;
	letter-spacing: normal;
	color: #FFF;
	margin-right: 10px;
`;

export const UserDrop = styled.div`
	font-size: 12px;
	color: #666b86;
`;

export const MenuContainer = styled.div`
	width: 300px;
	height: 179.5px;
	border-radius: 9px;
	background-color: #ffffff;
	box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.16);
	border: solid 1px rgba(102, 107, 134, 0.2);
	position: absolute;
	right: 87px;
	top: 58px;
	padding: 29px;
`;

export const MenuInnerContainer = styled.div`
	top: -30px;
	position: relative;
`;

export const Triangle = styled.div`
	position: relative;
	background: white;
	margin: auto;
	top: -42px;
	left: 50px;
	right: 0;
	width: 25px;
	height: 25px;
	transform: rotate(225deg);
	-webkit-transform: rotate(225deg);
	-moz-transform: rotate(225deg);
	-o-transform: rotate(225deg);
	-ms-transform: rotate(225deg);
	border-right: 1px solid rgba(102,107,134,.20);
	border-bottom: 1px solid rgba(102,107,134,.20);
`;

export const MenuLabel = styled.span`
	font-family: 'Poppins';
	font-size: 12px;
	font-weight: 500;
	font-style: normal;
	font-stretch: normal;
	line-height: normal;
	letter-spacing: 0.5px;
	color: #b1c7de;
	width: 43px;
	display: inline-block;
`;

export const MenuText = styled.span`
	font-family: 'Poppins';
	font-size: 12px;
	font-weight: normal;
	font-style: normal;
	font-stretch: normal;
	line-height: normal;
	letter-spacing: normal;
	color: #666b86;
`;