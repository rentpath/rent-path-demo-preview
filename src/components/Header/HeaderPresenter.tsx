import * as React from 'react';
import { Container, UserContainer, UserText, UserDrop, MenuContainer, 
         MenuInnerContainer, MenuLabel, MenuText, Triangle } from './HeaderStyles';

interface HeaderPresenterProps {
    user:string;
}

const HeaderPresenter = (props: HeaderPresenterProps) => {
    const logo = require('./../../assets/img/logo.jpg');
    return (
        <Container>
                 <UserContainer>
                    <UserText>{`${props.user}`}</UserText>
                </UserContainer>
        </Container>              
        );
};   

export default HeaderPresenter;