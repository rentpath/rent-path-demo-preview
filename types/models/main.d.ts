declare type SourceStatus = {
	sourceId: number,
	sourceInternalName: string,
	sourceName: string,
	status: string
}

declare type PropertyList = {
	properties: DemoPreviewListItem[],
	totalNumber: number,
	propertyListLoaded: boolean,
	pageUrl: string
}

declare type PropertyListItem = {
	id: number,
	listingId: number,
	name: string,
	manager: string,
	location: string,
	status: string,
	sourceStatus: SourceStatus[]
}

declare type DemoPreviewListItem = {
	id: number,
	listingId: number,
	clientName: string,
	pmc: string,
	city: string,
	state: string,
	address: string,
	previewUrl: string,
	leadmailUrl: string,
}

declare type LocationOptions = {
	states: State[],
	countries: Country[],
	timeZones: TimeZone[],
}